import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route } from "react-router-dom";


import Home from "./pages/Home/Home";
import Login from "./pages/Login/Login";
import EditTodo from "./pages/EditTodo/EditTodo";
import NavBar from "./components/NavBar/NavBar";

export default class App extends Component {
    render() {
        return (
            <Router>
                <div className='container'>
                    <NavBar />
                </div>
                <Route path='/' exact component={Home} />
                <Route path='/edit/:id' exact component={EditTodo} />
                <Route path='/create' exact component={EditTodo} />
                <Route path='/login' exact component={Login} />
            </Router>
        );
    }
}
