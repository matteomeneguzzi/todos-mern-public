import React, { Component } from "react";
import { Button, Col, Form, Row } from "react-bootstrap";
import TodoData from "../../api/TodosData";

import { Todo } from "../../types/Types";

interface Props {
	history: any;
	match: {
		isExact: boolean;
		params: { id: string };
		path: string;
		url: string;
	};
}

interface State {
	todo: Todo;
	loading: boolean;
}

export default class EditTodo extends Component<Props, State> {
    state: State = {
        todo: {
            description: "",
            responsible: "",
            priority: "",
            completed: false,
        },
        loading: true,
    };

    fetchTodo = async () => {
        let {todo} = this.state;
        const {match} = this.props;
        if (match.params.id){
            const id = this.props.match.params.id;
            try { 
                
                todo = await TodoData.getTodo(id).then(res => todo = res.data as Todo);
                this.setState({
                    todo: todo,
                    loading: false,
                });
            } catch (error) {
                console.log(error);
            }
        }};

    componentDidMount = async () => {
        await this.fetchTodo();
    };

    onChange = (e: any) => {
        const { value, name, type, checked } = e.target;

        if (type === "radio") {
            const textContent = e.target.nextElementSibling.textContent;
            this.setState({
                ...this.state,
                todo: {
                    ...this.state.todo,
                    [name]: textContent,
                },
            });
        } else {
            this.setState({
                ...this.state,
                todo: {
                    ...this.state.todo,
                    [name]: type === "checkbox" ? checked : value,
                },
            });
        }
    };

    onSubmit = async (e: any) => {
        const { todo } = this.state;
        const { match } = this.props;

        e.preventDefault();

        if(todo.description === '' && todo.responsible === '') return;
        if (match.params.id) {
            await TodoData.updateTodo(todo);
        } else {
            await TodoData.createTodo(todo);
        }
        this.props.history.push("/");
    };

    render() {
        const { todo, loading } = this.state;
        if (!todo || loading) return <></>;
        
        const { description, responsible } = todo;

        return (
            <div className='container container--custom'>
                <h3>{this.props.match.params.id ? "Edit Todo" : "Create Todo"}</h3>
                <Form onSubmit={this.onSubmit}>
                    <Form.Group className='mb-3'>
                        <Form.Label htmlFor='description'>Description:</Form.Label>
                        <Form.Control
                            required
                            type='text'
                            id='description'
                            placeholder='Take note...'
                            name='description'
                            value={description}
                            onChange={this.onChange}
                        ></Form.Control>
                    </Form.Group>

                    <Form.Group className='mb-3'>
                        <Form.Label htmlFor='responsible'>Responsible:</Form.Label>
                        <Form.Control
                            required
                            type='text'
                            id='responsible'
                            placeholder='Take note...'
                            name='responsible'
                            value={responsible}
                            onChange={this.onChange}
                        ></Form.Control>
                    </Form.Group>

                    <fieldset>
                        <Form.Group as={Row} className='mb-3 d-flex align-items-center'>
                            <Form.Label column sm={2}>
								Priority:
                            </Form.Label>
                            <Col sm={10}>
                                <Form.Check
                                    required
                                    inline
                                    type='radio'
                                    label='Low'
                                    name='priority'
                                    id='priority'
                                    onChange={this.onChange}
                                    checked={todo.priority === "Low" ? true : false}
                                />
                                <Form.Check
                                    required
                                    inline
                                    type='radio'
                                    label='Medium'
                                    name='priority'
                                    id='priority'
                                    onChange={this.onChange}
                                    checked={todo.priority === "Medium" ? true : false}
                                />
                                <Form.Check
                                    required
                                    inline
                                    type='radio'
                                    label='High'
                                    name='priority'
                                    id='priority'
                                    onChange={this.onChange}
                                    checked={todo.priority === "High" ? true : false}
                                />
                            </Col>
                        </Form.Group>
                    </fieldset>

                    <Form.Group className='mb-3'>
                        <Form.Check
                            type='checkbox'
                            id='completed'
                            label='Completed'
                            name='completed'
                            checked={todo.completed === true ? true : false}
                            onChange={this.onChange}
                        ></Form.Check>
                    </Form.Group>
                    <Button type='submit'>{this.props.match.params.id ? "Edit Todo" : "Create Todo"}</Button>
                </Form>
            </div>
        );
    }
}
