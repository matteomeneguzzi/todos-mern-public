import React, { Component } from "react";

import TodoList from "../../components/TodosList/TodoList";


export default class Home extends Component {
    render() {
        return (
            <div className='container container--custom'>
                <TodoList />
               
            </div>
        );
    }
}
