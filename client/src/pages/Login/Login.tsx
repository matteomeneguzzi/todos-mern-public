import React, { Component } from 'react';


// CSS
import './styles.css';

// Type
import {User} from "../../types/Types";
import { Button, Form } from 'react-bootstrap';

interface Props {
    history: any;
}

interface State {
    user: User;
}


export default class Login extends Component<Props, State> {
    state: State = {
        user: {
            username: "",
            password: ""
        }
    };


    onChange = (e: any) => {
        const { value, name, type, checked } = e.target;
        this.setState({
            user: {
                ...this.state.user,
                [name]: type === "checkbox" ? checked : value,
            },
        });
    };

    onSubmit = (e: any) => {
		
        e.preventDefault();

        this.setState({
            user: {
                username: "",
                password: ""
            }
        });

        this.props.history.push("/");
    };

    render() {
        const {username, password} = this.state.user;

        return (
            <div className='container container--login'><h3>Login Page</h3>
                <Form onSubmit={this.onSubmit}>
                    <Form.Group className='mb-3'>
                        <Form.Label htmlFor='username'>Username:</Form.Label>
                        <Form.Control
                            required
                            type='text'
                            id='username'
                            placeholder='Your username...'
                            name='username'
                            value={username}
                            onChange={this.onChange}
                        ></Form.Control>
                    </Form.Group>

                    <Form.Group className='mb-3'>
                        <Form.Label htmlFor='password'>Password:</Form.Label>
                        <Form.Control
                            required
                            type='text'
                            id='password'
                            placeholder='Your password...'
                            name='password'
                            value={password}
                            onChange={this.onChange}
                        ></Form.Control>
                    </Form.Group>

                    
                    <Button type='submit'>Login</Button>
                </Form></div>
        );
    }
}
