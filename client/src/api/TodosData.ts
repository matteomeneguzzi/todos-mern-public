import { Todo } from "../types/Types";
import api from "./Api";
import { ApiResponse } from "apisauce";

export default class Todos {
    static getTodos = async (): Promise<ApiResponse<Todo[]>> => {
        return api.get(`/todos`);
    };

    static getTodo = async (id: string): Promise<ApiResponse<Todo>> => {
        return api.get(`/todos/${id}`);
    };

    static createTodo = async (todo: Todo): Promise<ApiResponse<Todo>> => {
        return api.post(`/todos/create`, todo);
    };

    static updateTodo = async (todo: Todo): Promise<ApiResponse<Todo>> => {
        return api.put(`/todos/edit/${todo._id}`, todo);
    };

    static deleteTodo = async (id: string): Promise<ApiResponse<Todo>> => {
        return api.delete(`/todos/${id}`);
    };
}
