import {
	create,
	ApisauceInstance,
	ApiErrorResponse,
	ApiResponse,
} from "apisauce";
import config from "../config/config";
import { Unauthorized } from "../errors";

let api: ApisauceInstance;

if (api! === undefined) {
	api = create({
		baseURL:
			!process.env.NODE_ENV || process.env.NODE_ENV === "development"
				? config.endpoint
				: config.endpoint,
		timeout: 10000,
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json",
		},
	});
}

// export const responseErrorCheck = <T = any>(res: ApiResponse<T>) => {
// 	if (res?.status === 0 || res?.problem === "UNKNOWN_ERROR") {
// 		throw new Error(
// 			"Make sure you are connected in VPN and that you have access to an internet connection."
// 		);
// 	}

// 	if (res && (res.status === 200 || res.status === 204)) {
// 		return res.data as T;
// 	}

// 	if (res.status === 401 && res.config?.url === "/login") {
// 		throw new Error("Credential error");
// 	}

// 	if (res.status === 401) {
// 		throw new Error("Not authorized");
// 	}

// 	if (res.status === 403) {
// 		throw new Unauthorized(res as ApiErrorResponse<any>);
// 	}

// 	if (res.status === 404) {
// 		throw new Error(res?.data?.message ?? "404 Error");
// 	}

// 	if (res?.status && res?.status >= 400) {
// 		const errorHasCode =
// 			res.data?.errorCode !== undefined || res.data?.messageCode !== undefined;
// 		throw new Error(
// 			`${res.data?.errorCode ?? res.data?.messageCode ?? ""}${
// 				errorHasCode ? ": " : ""
// 			}${res.data?.message}` ?? "Generic Error"
// 		);
// 	}

// 	if (res.data && res.data.error?.message) {
// 		throw new Error("Error");
// 	}

// 	if (!res || !res.data) {
// 		throw new Error("Malformed Response from server");
// 	}

// 	if (res.problem) {
// 		throw new Error("Error");
// 	}

// 	return res.data;
// };

export default api;
