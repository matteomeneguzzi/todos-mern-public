import React, { Component } from "react";
import { Button, Modal } from "react-bootstrap";
import TodoData from "../../api/TodosData";

import { Todo } from "../../types/Types";

import "./styles.css";

interface Props {
	show: boolean;
	data: Todo;
	handleModalState: () => void;
	getTodos: () => void;
}

export default class ModalComponent extends Component<Props> {
    deleteTodo = async () => {
        const { _id } = this.props.data;
        if (typeof _id === "string") {
            await TodoData.deleteTodo(_id);
            this.props.getTodos();
            this.props.handleModalState();
        }
    };

    render() {
        const { show } = this.props;
        return (
            <>
                {show && 
                    <div className='modal--container--position'>
                        <Modal.Dialog className='modal--position'>
                            <Modal.Header>
                                <Modal.Title>Modal title</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <p>
									Are you sure you want to delete {this.props.data.description}
                                </p>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant='success' onClick={this.props.handleModalState}>
									Back
                                </Button>
                                <Button variant='danger' onClick={() => this.deleteTodo()}>
									Delete
                                </Button>
                            </Modal.Footer>
                        </Modal.Dialog>
                    </div>
                }
            </>
        );
    }
}
