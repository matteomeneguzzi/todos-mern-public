import React from "react";
import { Component } from "react";
import { Navbar, Container, Nav } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

// CSS
import "./styles.css";

export default class NavBar extends Component {
    render() {
        return (
            <Navbar collapseOnSelect expand='lg' variant='light'>
                <Container>
                    <Navbar.Brand href='/'>
                        <img
                            src='logo192.png'
                            alt='Logo'
                            width='30'
                            height='30'
                            className='img-fluid navbar--img'
                        />
						MERN Todo List
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls='responsive-navbar-nav' />
                    <Navbar.Collapse id='responsive-navbar-nav'>
                        <Nav className='me-auto'>
                            <LinkContainer to='/'>
                                <Nav.Link>Home</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to='/create'>
                                <Nav.Link>Create Todo</Nav.Link>
                            </LinkContainer>
                        </Nav>
                        <Nav className='mr-auto'>
                            <LinkContainer to='/login'>
                                <Nav.Link>Login</Nav.Link>
                            </LinkContainer>
                           
                        </Nav>
                    </Navbar.Collapse>
					
                </Container>
            </Navbar>
        );
    }
}
