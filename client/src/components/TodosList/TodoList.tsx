import { ApiResponse } from "apisauce";
import React, { Component } from "react";
import { Triangle } from "react-loader-spinner";
import TodosData from "../../api/TodosData";
import { Todo, ApiResponseCustom } from "../../types/Types";
import TodoDetail from "../TodoDetail/TodoDetail";

// CSS
import "./styles.css";

interface State {
	todos: Todo[];
	loading: boolean;
}

export default class TodoList extends Component {
    state: State = {
        todos: [],
        loading: true,
    };

    fetchTodos = () => {
        try {
            TodosData.getTodos().then((res) => {
                this.setState({
                    todos: res.data,
                    loading: false,
                });
            });
        } catch (err) {
            console.log(err);
        }
    };

    componentDidMount() {
        setTimeout(this.fetchTodos, 1000);
    }

    render() {
        const { todos, loading } = this.state;
        console.log(this.state.todos);

        const todoList = todos.map((todo, index) => {
            return <TodoDetail getTodos={this.fetchTodos} todo={todo} key={index} />;
        });

        return (
            <>
                {loading === true ? (
                    <div className='container--loader d-flex justify-content-center align-items-center'>
                        <Triangle height='100' width='100' color='dodgerblue' />
                    </div>
                ) : (
                    <div>
                        <div className='todo--title--container d-flex justify-content-between '>
                            <div className='todo--title col'>Description</div>
                            <div className='todo--title col'>Responsible</div>
                            <div className='todo--title col'>Priority</div>
                            <div className='todo--title col'>Actions</div>
                        </div>

                        {todoList}
                    </div>
                )}
            </>
        );
    }
}
