import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Todo } from "../../types/Types";

// Components
import ModalComponent from "../Modal/ModalComponent";

// CSS
import "./styles.css";

interface Props {
	todo: Todo;
	getTodos: () => void;
}

interface State {
	show: boolean;
	todo: Todo;
}

export default class TodoDetail extends Component<Props> {
    state: State = {
        show: false,
        todo: this.props.todo,
    };

    handleModal = () => {
        this.setState({
            show: !this.state.show,
        });
    };

    render() {
        const todo = this.props.todo;
        const { show } = this.state;
        return (
            <>
                <div className='todo--container d-flex align-items-center'>
                    <div className={todo.completed === true ? "completed col" : "col"}>
                        {todo.description}
                    </div>
                    <div className='col'>{todo.responsible}</div>
                    <div className='col'>{todo.priority}</div>
                    <div className='action d-flex col'>
                        <Link className='mr-3' to={`/edit/${todo._id}`}>
                            <Button>Edit</Button>
                        </Link>
                        <Button variant='danger' onClick={this.handleModal}>
							Delete
                        </Button>
                    </div>
                </div>

                <ModalComponent
                    data={todo}
                    show={show}
                    handleModalState={this.handleModal}
                    getTodos={this.props.getTodos}
                />
            </>
        );
    }
}
