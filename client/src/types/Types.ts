export interface Todo {
	description?: string;
	responsible?: string;
	priority?: string;
	completed?: boolean;
	_id?: string;
}

export interface ApiResponseCustom<T> {
	errorMessage?: string;
	responseCode?: string;
	data?: T[];
}

export interface User {
	username: string;
	password: string;
}