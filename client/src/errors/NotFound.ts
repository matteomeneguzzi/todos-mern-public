import ApiError from "./ApiError";

class NotFound extends ApiError {
	statusCode = 404;

	message = "The server has not found anything matching the Request-URI.";
}

export default NotFound;
