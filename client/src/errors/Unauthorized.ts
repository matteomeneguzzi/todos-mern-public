import ApiError from "./ApiError";

class Unauthorized extends ApiError {
	statusCode = 401;

	message =
		"Similar to 403 Forbidden, but specifically for use when authentication is possible but has failed or not yet been provided.";
}

export default Unauthorized;
