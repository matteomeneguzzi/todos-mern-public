class Forbidden extends Error {
	statusCode = 403;

	message =
		"The server understood the request, but is refusing to fulfill it. Authorization will not help and the request SHOULD NOT be repeated.";
}

export default Forbidden;
