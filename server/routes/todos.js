import express from "express";
import {
	getTodos,
	createTodo,
	editTodo,
	getTodo,
	deleteTodo,
} from "../controllers/todos.js";

const router = express.Router();

router.get("/", getTodos);
router.get("/:id", getTodo);
router.post("/create", createTodo);
router.put("/edit/:id", editTodo);
router.delete("/:id", deleteTodo);
export default router;
