import express from "express";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import cors from "cors";

import todoRoutes from "./routes/todos.js";

const app = express();

app.use(cors());
app.use(express.json());

app.use("/todos", todoRoutes);

const CONNECTION_URL =
	"mongodb+srv://javascriotmastery:javascriptmastery123@cluster0.2rbfa.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
const PORT = process.env.PORT || 4000;

mongoose
	.connect(CONNECTION_URL, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	})
	.then(() =>
		app.listen(PORT, () => console.log(`Server running on port: ${PORT}`))
	)
	.catch((error) => console.log(error));

const connection = mongoose.connection;

connection.once("open", function () {
	console.log("Mongo DB connected successfully");
});

// todoRoutes.route("/").get((req, res) => {
// 	Todo.find((err, todos) => {
// 		try {
// 			res.status(200).json(todos);
// 		} catch (err) {
// 			res.status(404).json({ message: err.message });
// 		}
// 	});
// });

// todoRoutes.route("/:id").get((req, res) => {
// 	let id = req.params.id;
// 	Todo.findById(id, function (err, todo) {
// 		res.json(todo);
// 	});
// });

// todoRoutes.route("/add").post((req, res) => {
// 	let todo = new Todo(req.body);
// 	console.log(todo);
// 	todo
// 		.save()
// 		.then((todo) => {
// 			res.status(200).json({ todo: "todo added succesfully" });
// 		})
// 		.catch((err) => {
// 			res.status(400).send("adding new todo failed");
// 		});
// });

// todoRoutes.route("/edit/:id").put((req, res) => {
// 	console.log(req);
// 	let id = req.params.id;
// 	Todo.findById(id, (err, todo) => {
// 		if (!todo) {
// 			res.status(404).send("data not found");
// 		} else {
// 			todo.description = req.body.description;
// 			todo.responsible = req.body.responsible;
// 			todo.priority = req.body.priority;
// 			todo.completed = req.body.completed;

// 			todo
// 				.save()
// 				.then((todo) => {
// 					res.json({ todo: "Todo updated" });
// 				})
// 				.catch((err) => {
// 					res.status(400).send("Update not possible");
// 				});
// 		}
// 	});
// 	let todo = new Todo(req.body);
// 	todo
// 		.save()
// 		.then((todo) => {
// 			res.status(200).json({ todo: "todo added succesfully" });
// 		})
// 		.catch((err) => {
// 			res.status(400).send("adding new todo failed");
// 		});
// });

// app.use("/todos", todoRoutes);

// app.get("/", (req, res) => {
// 	Todo.find((err, todos) => {
// 		try {
// 			res.status(200).json(todos);
// 		} catch (err) {
// 			res.status(404).json({ message: err.message });
// 		}
// 	});
// });
