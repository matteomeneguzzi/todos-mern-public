import mongoose from "mongoose";

const todoSchema = new mongoose.Schema({
	description: String,
	responsible: String,
	priority: String,
	completed: Boolean,
});

const TodoMessage = mongoose.model("TodoMessage", todoSchema);

export default TodoMessage;
