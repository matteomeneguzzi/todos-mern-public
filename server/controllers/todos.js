import mongoose from "mongoose";
import TodoMessage from "../models/todo.model.js";

export const getTodos = async (req, res) => {
	try {
		const todoMessages = await TodoMessage.find();
		res.status(200).json(todoMessages);
	} catch (error) {
		res.status(404).json({ message: error.message });
	}
};

export const getTodo = async (req, res) => {
	const { id } = req.params;
	try {
		const todoMessage = await TodoMessage.findById(id);
		res.status(200).json(todoMessage);
	} catch (error) {
		res.status(404).json({ message: error.message });
	}
};

export const createTodo = async (req, res) => {
	
	const todo = req.body;
	const newTodo = new TodoMessage(todo);

	try {
		await newTodo.save();
		res.status(201).json(newTodo);
	} catch (error) {
		res.status(409).json({ message: error.message });
	}
};

export const editTodo = async (req, res) => {
	TodoMessage.findByIdAndUpdate(
		req.params.id,
		{
			description: req.body.description,
			responsible: req.body.responsible,
			priority: req.body.priority,
			completed: req.body.completed,
		},
		{ new: true }
	)
		.then((data) => {
			if (!data) {
				return res.status(404).send({
					message: "Message not found with id " + req.params.id,
				});
			}
			res.send(data);
		})
		.catch((err) => {
			if (err.kind === "ObjectId") {
				return res.status(404).send({
					message: "Message not found with id " + req.params.id,
				});
			}
			return res.status(500).send({
				message: "Error updating todo with id " + req.params.id,
			});
		});
};

export const deleteTodo = async (req, res) => {
	TodoMessage.findByIdAndRemove(req.params.id)
		.then((data) => {
			if (!data) {
				return res.status(404).send({
					message: "Message not found with id " + req.params.id,
				});
			}
			res.send(data);
			
		})
		.catch((err) => {
			if (err.kind === "ObjectId") {
				return res.status(404).send({
					message: "Message not found with id " + req.params.id,
				});
			}
			return res.status(500).send({
				message: "Could not delete todo with id " + req.params.id,
			});
		});
};
